#!/usr/bin/sh

indir=""
outdir=""

if [ "$#" -le "1" ]
then
	echo "too few arguments; Syntax is ./make.sh \$INPUTDIR \$OUTPUTDIR"
	exit 1
fi

indir="$1"
outdir="$2"
tmpfile1="$(mktemp /tmp/timeline-XXXXXXXXXXXXXX.txt)"
tmpfile2="$(mktemp /tmp/timeline-XXXXXXXXXXXXXX.txt)"
stuff="
<!DOCTYPE HTML>
<html>
	<head>
		<base target=\"_top\">
	</head>
	<body>
		<div style=\"position: static; float: left; overflow: hidden; background-color: yellow;\">
			<a href=\"./index.html\">Übersicht</a>
			<a href=\"/index.html\">Index</a>
		</div>
		<div style=\"position: static; float: left; overflow: hidden; background-color: white;\">
			Die Inhalte wurde nach besten Wissen und Gewissen erstellt, können aber dennoch Missverständnisse oder Fehler enthalten. Es wird keine Haftung oder Verantwortung übernommen!
			<a href=\"/impressum.html\">Impressum</a><a href=\"/privacy.html\">Privacy</a><a href=\"/timeline/index.html\">index</a>
		</div>
	</body>
</html>"

echo "$stuff" > "$outdir/title.html"

echo "" > "$outdir/index.html"
echo "<!DOCTYPE HTML>" >> "$outdir/index.html"
echo "<html>" >> "$outdir/index.html"
echo "	<head>" >> "$outdir/index.html"
echo "		<meta charset=\"UTF-16\">" >> "$outdir/index.html"
echo "		<base target=\"_top\">" >> "$outdir/index.html"
echo "		<title>Übersicht</title>" >> "$outdir/index.html"
echo "	</head>" >> "$outdir/index.html"
echo "	<body>" >> "$outdir/index.html"
echo "	<ul>" >> "$outdir/index.html"
echo "		<li><a href=\"all.html\">all</a></li>" >> "$outdir/index.html"

for i in $(find "$indir" -name '*.hist')
do
	if [ -d "$i" ]
	then
		echo "DIR"
	else
		outfile="$(echo "$outdir/${i#$indir}" | sed 's/.hist/.html/')"
		echo "Parsing $i --> $outfile"
		./timeline.py create "$i" "$tmpfile1"
		echo "$(echo "${i#$indir}" | sed 's/.hist//' | sed 's/_/ /' | sed 's/$indir//'):$i" >> "$tmpfile2"

		echo "		<li><a href=\"$(echo "${i#$indir}" | sed 's/.hist/.html/')\">$(echo "${i#$indir}" | sed 's/.hist//' | sed 's/_/ /' | sed 's/$indir//')</a></li>" >> "$outdir/index.html"

		echo "" > "$outfile"
		echo "<!DOCTYPE HTML>" >> "$outfile"
		echo "<html>" >> "$outfile"
		echo "	<head>" >> "$outfile"
		echo "		<meta charset=\"UTF-8\">" >> "$outfile"
		echo "		<base target=\"_top\">" >> "$outfile"
		echo "		<title>Übersicht</title>" >> "$outfile"
		echo "	</head>" >> "$outfile"
		echo "	<body>" >> "$outfile"
		cat "$tmpfile1" >> "$outfile"
		echo "	</body>" >> "$outfile"
		echo "</html>" >> "$outfile"
	fi
done

./timeline.py batch create "$tmpfile2" "$tmpfile1"
echo "" > "$outdir/all.html"
echo "<!DOCTYPE HTML>" >> "$outdir/all.html"
echo "<html>" >> "$outdir/all.html"
echo "	<head>" >> "$outdir/all.html"
echo "		<meta charset=\"UTF-8\">" >> "$outdir/all.html"
echo "		<base target=\"_top\">" >> "$outdir/all.html"
echo "		<title>Alle zusammen</title>" >> "$outdir/all.html"
echo "	</head>" >> "$outdir/all.html"
echo "	<body>" >> "$outdir/all.html"
cat "$tmpfile1" >> "$outdir/all.html"
echo "	</body>" >> "$outdir/all.html"
echo "</html>" >> "$outdir/all.html"

echo "	</ul>" >> "$outdir/index.html"
echo "	<iframe src=\"./title.html\"></iframe>" >> "$outdir/index.html"
echo "	</body>" >> "$outdir/index.html"
echo "</html>" >> "$outdir/index.html"

rm "$tmpfile1"
rm "$tmpfile2"
