#!/usr/bin/python3

import sys
from datetime import datetime
import operator
import numpy as np
import markdown

# some ideas and informations about the functions

class TimePoint:
    time = 0
    more_than_year = 0
    linenumber = 0
    y_pos = 0
    calc_y_pos = 0
    abs_y_pos = 0

    def __init__(self):
        self.starttime = 0
        self.endtime = 0
        self.more_than_year = 0
        self.y_pos = 0
        self.calc_y_pos = 0
        self.abs_y_pos = 0

    def __lt__(self, other):
        return self.time < other.time

    def __eq__(self, other):
        return self.time == other.time

class Event:
    title = ""
    text = ""
    indent = 0
    color = ""
    origin = ""
    starttime = 0

    def __init__(self):
        self.title = ""
        self.text = ""
        self.indent = 0
        self.x_pos = 0
        self.y_pos = 0
        self.width = 0
        self.height = 0
        self.color = ""
        self.origin = ""
        self.starttime = 0

class Period:
    title = ""
    text = ""
    indent = 0
    color = ""
    origin = ""
    starttime = 0
    endtime = 0

    def __init__(self):
        self.title = ""
        self.text = ""
        self.indent = 0
        self.x_pos = 0
        self.y_pos = 0
        self.width = 0
        self.height = 0
        self.color = ""
        self.origin = ""
        self.starttime = 0
        self.endtime = 0


def OperatorEvent(_i):
    return _i.starttime.time
def OperatorPeriod(_i):
    return _i.starttime.time
def OperatorPoint(_i):
    return _i.time

time_letter_count = 20
info_period = []
info_time = []
info_point = []
brg_colors = ["AliceBlue","AntiqueWhite","Aqua","Aquamarine","Azure","Beige","Bisque","Black","BlanchedAlmond","Blue","BlueViolet","Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkCyan","DarkGoldenRod","DarkGray","DarkGrey","DarkGreen","DarkKhaki","DarkMagenta","DarkOliveGreen","DarkOrange","DarkOrchid","DarkRed","DarkSalmon","DarkSeaGreen","DarkSlateBlue","DarkSlateGray","DarkSlateGrey","DarkTurquoise","DarkViolet","DeepPink","DeepSkyBlue","DimGray","DimGrey","DodgerBlue","FireBrick","FloralWhite","ForestGreen","Fuchsia","Gainsboro","GhostWhite","Gold","GoldenRod","Gray","Grey","Green","GreenYellow","HoneyDew","HotPink","IndianRed","Indigo","Ivory","Khaki","Lavender","LavenderBlush","LawnGreen","LemonChiffon","LightBlue","LightCoral","LightCyan","LightGoldenRodYellow","LightGray","LightGrey","LightGreen","LightPink","LightSalmon","LightSeaGreen","LightSkyBlue","LightSlateGray","LightSlateGrey","LightSteelBlue","LightYellow","Lime","LimeGreen","Linen","Magenta","Maroon","MediumAquaMarine","MediumBlue","MediumOrchid","MediumPurple","MediumSeaGreen","MediumSlateBlue","MediumSpringGreen","MediumTurquoise","MediumVioletRed","MidnightBlue","MintCream","MistyRose","Moccasin","NavajoWhite","Navy","OldLace","Olive","OliveDrab","Orange","OrangeRed","Orchid","PaleGoldenRod","PaleGreen","PaleTurquoise","PaleVioletRed","PapayaWhip","PeachPuff","Peru","Pink","Plum","PowderBlue","Purple","RebeccaPurple","Red","RosyBrown","RoyalBlue","SaddleBrown","Salmon","SandyBrown","SeaGreen","SeaShell","Sienna","Silver","SkyBlue","SlateBlue","SlateGray","SlateGrey","Snow","SpringGreen","SteelBlue","Tan","Teal","Thistle","Tomato","Turquoise","Violet","Wheat","WhiteSmoke","Yellow","YellowGreen"] 

seperator_fields = '@'
seperator_date = '>'
sign_anotherline = '$'
seperator_batch = ':'
sign_comment = '#'
sign_text = '€'

title_prefix = ""
category = 0

text = ""

font_size = 12
letter_width = int(font_size / 2)
min_dst = int(font_size + 2)
period_width = int(font_size * 2 + letter_width)
indent_pixels = int(period_width)

max_indent_period = 0
max_indent_time = 0
max_title_length = 0
min_gap = np.timedelta64(4294967296, 'D')
zero_gap = np.timedelta64(0, 'D')

size = 0

count_bars = 0
current_bar = 0
save_scaling = False
save_compressed = True
save_infos = True
save_log = False
save_bulk = False

title_space = 50


def RemoveDuplicates(_i):
    return _i

    l = []

    for i in range(0, len(_i) - 1):
        add = True
        for j in l:
            if _i[i] == j:
                add = False
        if add:
            l.append(_i[i])

    return l

def ScaleLinear(_v):
    return int(_v)

def ScaleLogarithm(_v):
    if (_v == 0):
        return 0
    return int(float(font_size) + 2 * np.lib.scimath.logn(1.03, float(abs(_v))))

def ScaleCompressed(_v):
    return int(float(font_size) + (abs(float(_v)) - float(font_size)) / (np.lib.scimath.logn(1.02, float(abs(_v) + 1e-8))))

def ScaleBulk(_v):
    return int(font_size)
        


# Load(_f):
#   The Load function attempts to load the file with the path _f
#   It loads the data to so-called timepoints which are just a class
#   After that, it assigns an event to each timepoint, there can be multiple timepoints for one event
def Load(_f):
    global info_time
    global info_period
    global info_point
    global text

    m = TimePoint()
    e = Event()
    p = Period()
    t = ""
    x = ""
    last_period = False
    linenumber = 0
    s = False

    print("Loading " + str(_f) + "...")

    f = open(str(_f), "r")


    for line in f:
        linenumber += 1
        try:
            # print("trying : " + str(line))
            line = line[:-1] + "  \n"
            if line[0] == sign_text:
                if line[1] == '-':
                    line = "€\n" + line[1:]

                text += line[1:-1] + "  \n"
                continue

            if len(line.strip()) > 0 and line[0] != sign_comment:
                if line[0] == sign_anotherline:
                    # Markdown stuff 
                    if line[1] == '-':
                        line = "$\n" + line[1:]

                    if last_period == True:
                        info_period[-1].text += line[1:]
                    else:
                        info_time[-1].text += line[1:]
                    continue

                t = line.split(seperator_fields)[1]
                while t[0] == ' ' and len(t) > 0:
                    t = t[1:]
                if title_prefix != "":
                    t = str(title_prefix) + " - " + t

                x = line.split(seperator_fields)[2]

                line = line.strip()
                lt = line.split(seperator_fields)[0]

                if seperator_date in lt:
                    last_period = True

                    if "-" in lt.split(seperator_date)[0]:
                        m.time = np.datetime64(lt.split(seperator_date)[0], 'D')
                        m.more_than_year = 1
                    else:
                        m.time = np.datetime64(lt.split(seperator_date)[0], 'D')
                        m.more_than_year = 0
                    m.linenumber = linenumber

                    s = True
                    for i in info_point:
                        if m.time == i.time and m.more_than_year == i.more_than_year:
                            # print("A: REUSE: " + str(m.time) + " == " + str(i.time) + " and " + str(m.more_than_year) + " == " + str(i.more_than_year) + " in line " + str(linenumber) + " from line " + str(i.linenumber))
                            m = i
                            s = False
                            break
                    if s:
                        info_point.append(m)
                    p.starttime = m
                    m = TimePoint()

                    if "-" in lt.split(seperator_date)[1]:
                        m.time = np.datetime64(lt.split(seperator_date)[1], 'D')
                        m.more_than_year = 1
                    else:
                        m.time = np.datetime64(lt.split(seperator_date)[1], 'D')
                        m.more_than_year = 0
                    m.linenumber = linenumber

                    s = True
                    for i in info_point:
                        if m.time == i.time and m.more_than_year == i.more_than_year:
                            # print("B: REUSE: " + str(m.time) + " == " + str(i.time) + " and " + str(m.more_than_year) + " == " + str(i.more_than_year) + " in line " + str(linenumber) + " from line " + str(i.linenumber))
                            m = i
                            s = False
                            break
                    if s:
                        info_point.append(m)
                    p.endtime = m

                    p.title = t
                    p.text = x
                    p.color = brg_colors[category  % len(brg_colors)]


                    info_period.append(p)
                    m = TimePoint()
                    p = Period()
                else:
                    last_period = False

                    if "-" in lt:
                        m.time = np.datetime64(lt, 'D')
                        m.more_than_year = 1
                    else:
                        m.time = np.datetime64(lt, 'D')
                        m.more_than_year = 0
                    m.linenumber = linenumber

                    s = True
                    for i in info_point:
                        if m.time == i.time and m.more_than_year == i.more_than_year:
                            # print("C: REUSE: " + str(m.time) + " == " + str(i.time) + " and " + str(m.more_than_year) + " == " + str(i.more_than_year) + " in line " + str(linenumber) + " from line " + str(i.linenumber))
                            m = i
                            s = False
                            break
                    if s:
                        info_point.append(m)

                    e.title = t
                    e.text = x
                    e.starttime= m
                    e.color = brg_colors[category  % len(brg_colors)]

                    info_time.append(e)
                    m = TimePoint()
                    e = Event()
        except IndexError:
            print("Error in line " + str(linenumber) + ": " + str(line))

    f.close()

def PreConfig():
    global info_time
    global info_period
    global info_point
    global timespan
    global max_indent_period
    global max_indent_time
    global max_title_length
    global min_gap
    global size
    global count_bars

    rep = False
    last_index = 0
    last_period = False
    last_end = False
    time_to_last = np.timedelta64(4294967296, 'D')
    s1 = 0
    s2 = 0

    if len(info_point) <= 0 or len(info_period) + len(info_time) <= 0:
        return

    info_point.sort(key=OperatorPoint)
    info_period.sort(key=OperatorPeriod)
    info_time.sort(key=OperatorEvent)

    ############################################################################################################################################
    # calculate timespan and min_gap
    timespan = info_point[-1].time - info_point[0].time
    for i in range(1, len(info_point)):
        if info_point[i].time - info_point[i - 1].time < min_gap and info_point[i].time - info_point[i - 1].time > zero_gap:
            min_gap = info_point[i].time - info_point[i - 1].time

    ############################################################################################################################################
    # calculate indent and max_indent_period
    for i in range(0, len(info_period)):
        info_period[i].indent = 0
        rep = True
        while rep:
            rep = False
            for j in range(0, i):
                # print(str(info_period[i].starttime.time) + " <= " + str(info_period[j].endtime.time) + " and " + str(info_period[i].starttime.time) + " >= " + str(info_period[j].starttime.time) + " and " + str(info_period[i].indent) + " == " + str(info_period[j].indent) + ":" + str(info_period[i].title) + ":" + info_period[j].title)
                if info_period[i].starttime.time >= info_period[j].starttime.time and info_period[i].starttime.time <= info_period[j].endtime.time and info_period[i].indent == info_period[j].indent:
                    info_period[i].indent += 1
                    rep = True
                    break
        if info_period[i].indent > max_indent_period:
            max_indent_period = info_period[i].indent

    ############################################################################################################################################
    # calculate indent and max_indent_time and max_title_length
    for i in range(0, len(info_time)):
        for j in range(0, len(info_period)):
            if info_time[i].indent <= info_period[j].indent and info_time[i].starttime.time >= info_period[j].starttime.time and info_time[i].starttime.time <= info_period[j].endtime.time:
                info_time[i].indent = info_period[j].indent + 1

        if info_time[i].indent > max_indent_time:
            max_indent_time = info_time[i].indent
        if len(info_time[i].title) > max_title_length:
            max_title_length = len(info_time[i].title)

    ############################################################################################################################################
    # calculate width

    # 16 characters time maximum --> 16*4
    # s1: max indent in pixels of periods
    # s2: max indent in pixels of time
    s1 = max_indent_period * indent_pixels
    s2 = max_indent_time * indent_pixels + max_title_length * letter_width
    size = max(s1, s2)

    # Add size of time
    size += time_letter_count * letter_width

    # add size of timeline
    size += 12

    # Add size of space between border and text / ...
    size += 2 * font_size

    # Add size of space between text / period and timeline
    size += 2 * letter_width

    ############################################################################################################################################
    # calculate count
    if save_scaling:
        count_bars += 1
    if save_compressed:
        count_bars += 1
    if save_log:
        count_bars += 1
    if save_bulk:
        count_bars += 1
    if save_infos:
        count_bars += 1

    ############################################################################################################################################
    # calculate y distance
    info_point[0].y_pos = 0
    for i in range(1, len(info_point)):
        info_point[i].y_pos = int(abs(float((info_point[i].time - info_point[i - 1].time) / min_gap) * float(font_size)))
                

def SaveTimeLine(_h, _c, _n, _f2, _s = 0):
    global info_point

    if len(info_point) <= 0 or len(info_period) + len(info_time) <= 0:
        return (_h, _c, 0)

    max_ei = 0
    last_y = 0
    cnt = 0
    html = ""
    css = ""

    h1 = ""

    max_indent = 0


    _c += "    *\n"
    _c += "    {\n"
    _c += "        word-wrap: break-word;\n"
    _c += "    }\n"
    _c += "\n"
    _c += "    .timeline_popup_" + str(_n) + "\n"
    _c += "    {\n"
    _c += "        position: fixed;\n"
    _c += "        top: 20vh;\n"
    _c += "        left: 20vw;\n"
    _c += "        width: 60vw;\n"
    _c += "        height: 60vh;\n"
    _c += "        background-color: lightgrey;\n"
    _c += "        border-width: 0px;\n"
    _c += "        overflow: auto;\n"
    _c += "        visibility: hidden;\n"
    _c += "        z-index: 10;\n"
    _c += "    }\n"
    _c += "\n"
    _c += "    .timeline_popup_" + str(_n) + "_close\n"
    _c += "    {\n"
    _c += "        position: relative;\n"
    _c += "        top: 0%;\n"
    _c += "        left: 0%;\n"
    _c += "        width: 5%;\n"
    _c += "        height: 10%;\n"
    _c += "        background-color: red;\n"
    _c += "        color: white;\n"
    _c += "        float: left;\n"
    _c += "        border-width: 0px;\n"
    _c += "        font-size: " + str(font_size) + "px;\n"
    _c += "    }\n"
    _c += "\n"
    _c += "    .timeline_popup_" + str(_n) + "_title\n"
    _c += "    {\n"
    _c += "        position: relative;\n"
    _c += "        top: 0%;\n"
    _c += "        left: 0%;\n"
    _c += "        width: 95%;\n"
    _c += "        height: 10%;\n"
    _c += "        background-color: lightgrey;\n"
    _c += "        color: black;\n"
    _c += "        float: left;\n"
    _c += "        border-width: 0px;\n"
    _c += "    }\n"
    _c += "\n"
    _c += "    .timeline_popup_" + str(_n) + "_content\n"
    _c += "    {\n"
    _c += "        position: relative;\n"
    _c += "        top: 0%;\n"
    _c += "        left: 0%;\n"
    _c += "        width: 100%;\n"
    _c += "        height: auto;\n"
    _c += "        background-color: lightgrey;\n"
    _c += "        color: black;\n"
    _c += "        overflow: auto;\n"
    _c += "        border-width: 0px;\n"
    _c += "    }\n"


    x_coord = int(font_size + letter_width + time_letter_count * letter_width)
    y_coord = int(12)
    last_y = int(y_coord)

    for i in info_point:
        i.calc_y_pos = _f2(i.y_pos)
        last_y = i.abs_y_pos = last_y + i.calc_y_pos

    for i in range(0, len(info_time)):

        # Calculate extra indent (ei) in case of multiple events at one date
        #TODO: use ei in PreConfig for correct max_indent calculation
        ei = 0
        for j in range(0, i):
            if info_time[i].starttime.time == info_time[j].starttime.time:
                ei += letter_width * (len(info_time[j].title) + 2 * font_size)

        if ei + info_time[i].indent * indent_pixels > max_ei:
            max_ei = ei  + info_time[i].indent * indent_pixels

        x_coord = int(font_size + 2 * letter_width + time_letter_count * letter_width + 12 + info_time[i].indent * indent_pixels + ei)
        y_coord = int(info_time[i].starttime.abs_y_pos)

        if x_coord + len(letter_width * info_time[i].title) > max_indent:
            max_indent = x_coord + len(letter_width * info_time[i].title)

        h1 += "       <a name=\"#timeline_popup_" + str(_n) + "_" + str(cnt) + "_back\">\n"

        y_coord += title_space

        if info_time[i].starttime.more_than_year > 0:
            h1 += "       <div style=\"position: absolute; top: " + str(y_coord - letter_width) + "px; left: " + str(font_size + (1 - info_time[i].starttime.more_than_year) * 15 * letter_width) + "px;\">" + str(np.datetime_as_string(info_time[i].starttime.time, unit='D')) + "</a></div>\n"
        else:
            h1 += "       <div style=\"position: absolute; top: " + str(y_coord - letter_width) + "px; left: " + str(font_size + (1 - info_time[i].starttime.more_than_year) * 15 * letter_width) + "px;\">" + str(np.datetime_as_string(info_time[i].starttime.time, unit='Y')) + "</a></div>\n"

        h1 += "       <div style=\"position: absolute; top: " + str(y_coord - letter_width) + "px; left: " + str(x_coord) + "px;\"><a href=\"#timeline_popup_" + str(_n) + "_"  + str(cnt) + "\">" + str(info_time[i].title) + "</a></div>\n"

        h1 += "       <div class=\"timeline_popup_" + str(_n) + "\" id=\"timeline_popup_" + str(_n) + "_" + str(cnt) + "\">\n"
        h1 += "           <a class=\"timeline_popup_" + str(_n) + "_close\" href=\"#timeline_popup_" + str(_n) + "_" + str(cnt) + "_back\">&times</a>\n"
        if info_time[i].starttime.more_than_year > 0:
            h1 += "           <div class=\"timeline_popup_" + str(_n) + "_title\"><center><h3>" + str(info_time[i].title) + " (" + str(np.datetime_as_string(info_time[i].starttime.time, unit='D')) + ")</h3></center></div>\n"
        else:
            h1 += "           <div class=\"timeline_popup_" + str(_n) + "_title\"><center><h3>" + str(info_time[i].title) + " (" + str(np.datetime_as_string(info_time[i].starttime.time, unit='Y')) + ")</h3></center></div>\n"
        h1 += "           <div class=\"timeline_popup_" + str(_n) + "_content\">" + str(markdown.markdown(info_time[i].text)) + "</div>\n"
        h1 += "       </div>\n"
        h1 += "\n"

        _c += "    #timeline_popup_" + str(_n) + "_" + str(cnt) + "\n"
        _c += "    {\n"
        _c += "        visibility: hidden;\n"
        _c += "    }\n"
        _c += "    #timeline_popup_" + str(_n) + "_" + str(cnt) + ":target\n"
        _c += "    {\n"
        _c += "        visibility: visible;\n"
        _c += "    }\n"
        _c += "\n"

        cnt += 1

    for i in info_period:
        height =   int(i.endtime.abs_y_pos - i.starttime.abs_y_pos)
        width =   int(indent_pixels)
        x_coord = int(font_size + 2 * letter_width  + 12 + time_letter_count * letter_width + i.indent * indent_pixels)
        y_coord = int(i.starttime.abs_y_pos)
        color = brg_colors[np.random.randint(0, len(brg_colors))]

        if i.indent * indent_pixels > max_ei:
            max_ei = i.indent * indent_pixels

        y_coord += title_space

        h1 += "       <a href=\"#timeline_popup_" + str(_n) + "_" + str(cnt) + "\"><div style=\"overflow: hidden; transform: rotate(-90deg); background-color: " + str(color) + "; position: absolute; height: " + str(width) + "px; width: " + str(height) + "px; top: " + str(y_coord - width / 2 + height / 2) + "px; left: " + str(x_coord - height / 2 + width / 2) + "px;\"><center>" + str(i.title) + "</center></div></a>\n"
        h1 += "       <a name=\"#timeline_popup_" + str(_n) + "_" + str(cnt) + "_back\">\n"

        if i.starttime.more_than_year > 0:
            h1 += "       <div name=\"#timeline_popup_" + str(_n) + "_" + str(cnt) + "_back\" style=\"position: absolute; top: " + str(title_space + i.starttime.abs_y_pos - letter_width) + "px; left: " + str(font_size + (1 - i.starttime.more_than_year) * 15 * letter_width) + "px;\">" + str(np.datetime_as_string(i.starttime.time, unit='D')) + "</div>\n"
        else:
            h1 += "       <div name=\"#timeline_popup_" + str(_n) + "_" + str(cnt) + "_back\" style=\"position: absolute; top: " + str(title_space + i.starttime.abs_y_pos - letter_width) + "px; left: " + str(font_size + (1 - i.starttime.more_than_year) * 15 * letter_width) + "px;\">" + str(np.datetime_as_string(i.starttime.time, unit='Y')) + "</div>\n"

        if i.endtime.more_than_year > 0:
            h1 += "       <div style=\"position: absolute; top: " + str(title_space + i.endtime.abs_y_pos - letter_width) + "px; left: " + str(font_size + (1 - i.endtime.more_than_year) * 15 * letter_width) + "px;\">" + str(np.datetime_as_string(i.endtime.time, unit='D')) + "</div>\n"
        else:
            h1 += "       <div style=\"position: absolute; top: " + str(title_space + i.endtime.abs_y_pos - letter_width) + "px; left: " + str(font_size + (1 - i.endtime.more_than_year) * 15 * letter_width) + "px;\">" + str(np.datetime_as_string(i.endtime.time, unit='Y')) + "</div>\n"

        h1 += "       <div class=\"timeline_popup_" + str(_n) + "\" id=\"timeline_popup_" + str(_n) + "_" + str(cnt) + "\">\n"
        h1 += "           <a class=\"timeline_popup_" + str(_n) + "_close\" href=\"#timeline_popup_" + str(_n) + "_" + str(cnt) + "_back\">&times</a>\n"
        if i.endtime.more_than_year > 0:
            h1 += "           <div class=\"timeline_popup_" + str(_n) + "_title\"><center><h3>" + str(i.title) + " (" + str(np.datetime_as_string(i.starttime.time, unit='D')) + " - " + str(np.datetime_as_string(i.endtime.time, unit='D')) + ")</h3></center></div>\n"
        else:
            h1 += "           <div class=\"timeline_popup_" + str(_n) + "_title\"><center><h3>" + str(i.title) + " (" + str(np.datetime_as_string(i.starttime.time, unit='Y')) + " - " + str(np.datetime_as_string(i.endtime.time, unit='Y')) + ")</h3></center></div>\n"
        h1 += "           <div class=\"timeline_popup_" + str(_n) + "_content\">" + str(markdown.markdown(i.text)) + "</div>\n"
        h1 += "       </div>\n"
        h1 += "\n"

        _c += "    #timeline_popup_" + str(_n) + "_" + str(cnt) + "\n"
        _c += "    {\n"
        _c += "        visibility: hidden;\n"
        _c += "    }\n"
        _c += "    #timeline_popup_" + str(_n) + "_" + str(cnt) + ":target\n"
        _c += "    {\n"
        _c += "        visibility: visible;\n"
        _c += "    }\n"
        _c += "\n"

        cnt += 1

    max_ei = int(font_size + 2 * letter_width + time_letter_count * letter_width + 12 + + max_title_length * letter_width + max_ei)
    x_coord = int(font_size + time_letter_count * letter_width + letter_width)
    y_coord = int(12)
    _h += "<div style=\"background-color: white; font-size: " + str(font_size) + "px; width: " + str(max_ei) + "px; left: " + str(_s) + "px; height: " + str(info_point[-1].abs_y_pos + 12) + "px; position: absolute; top: " + str(title_space) + "px; overflow; scroll\">\n"
    _h += "   <div style=\"position: absolute; background-color: black; top: " + str(title_space + y_coord) + "px; width: 12px; height: " + str(info_point[-1].abs_y_pos - 12) + "px; left: " + str(x_coord) + "px;\"></div>\n"
    _h += "   <div style=\"position: absolute; background-color: black; top: " + str(title_space + info_point[-1].abs_y_pos) + "px; width: 0px; height: 0px; left: " + str(x_coord) + "px; border-width: 6px; border-style: solid; border-color: black white white white;\"></div>\n"
    _h += "\n\n"
    _h += h1
    _h += "\n\n"
    _h += "</div>"

    return (_h, _c, max_ei)

def SaveInfos(_h, _c, _s):
    print("Saving Infos")
    width = 0
    md = markdown.markdown(text)

    for i in text.split("\n"):
        if len(i) * letter_width + 3 * font_size * i.count("\t") > width:
            width = len(i) * letter_width + 3 * font_size * i.count("\t")

    _h += "<div style=\"position: absolute; background-color: lightgrey; font-size: " + str(font_size) + "px; width: " + str(width) + "px; left: " + str(_s) + "px; top: " + str(title_space) + "px; padding: " + str(font_size) + "overflow: auto\">\n"
    _h += str(md)
    _h += "</div>"

    return (_h, _c, width)

def Save(_f):
    html = ""
    css = ""
    oldpos = 0
    width = 0

    html += "       <iframe style=\"position: absolute; left: 0px; top: 0px; width: 100%; height: " + str(title_space) + "px;\" src=\"./title.html\"></iframe>"

    if save_infos:
        html, css, width = SaveInfos(html, css, oldpos)
        oldpos += width
    if save_scaling:
        html, css, width = SaveTimeLine(html, css, "linear_scale", ScaleLinear, oldpos)
        oldpos += width
    if save_compressed:
        html, css, width = SaveTimeLine(html, css, "cmp_scale", ScaleCompressed, oldpos)
        oldpos += width
    if save_log:
        html, css, width = SaveTimeLine(html, css, "log_scale", ScaleLogarithm, oldpos)
        oldpos += width
    if save_bulk:
        html, css, width = SaveTimeLine(html, css, "bulk_scale", ScaleBulk, oldpos)
        oldpos += width

    f = open(str(_f), "w")
    f.write("<style>\n")
    f.write(css)
    f.write("</style>\n")
    f.write(html)
    f.close()


if len(sys.argv) < 2:
    print("Syntax is: ./timeline.py [create, batch]")
    exit(0)

if sys.argv[1] == "create":
    if len(sys.argv) < 4:
        print("too few arguments, syntax is: ./timeline.py create $INFILE $OUTFILE")
        exit(0)
    Load(sys.argv[2])
    print("Loaded")
    # print(" - " + str(len(info_time)) + " events")
    # print(" - " + str(len(info_period)) + " periods")
    # print(" - " + str(len(info_point)) + " timepoints")

    PreConfig()
    Save(sys.argv[3])
elif sys.argv[1] == "batch":
    if len(sys.argv) < 5:
        print("too few arguments, yntax is: ./timeline.py batch [create, save] $BATCHFILE $OUTFILE")
        exit(0)

    if sys.argv[2] == "create":
        f = open(str(sys.argv[3]), "r")

        for line in f:
            title_prefix = str(line.split(seperator_batch)[0])
            Load(line.split(seperator_batch)[1].strip())

        print("Loaded")
        # print(" - " + str(len(info_time)) + " events")
        # print(" - " + str(len(info_period)) + " periods")
        # print(" - " + str(len(info_point)) + " timepoints")

        PreConfig()
        Save(str(sys.argv[4]))
    else:
        print("Unrecognized command")

else:
    print("unrecognized command")










# def decrypt(field):
#     s = 0
#     for i in range(0,63):
#         if field[i] == 1:
#             s = s ^ i
#     return s
# 
# def crypt(field, target):
#     s = decrypt(field)
#     field[s ^ target] = 1 - field[s ^ target]
#     return field

